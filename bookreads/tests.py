from django.test import TestCase
from django.urls import reverse

from books.models import Book, BookReview
from users.models import CustomUser


class HomePageTestCase(TestCase):
    def test_paginated_list(self):
        book = Book.objects.create(title="Book1", description="desc1", isbn="111111")
        user = CustomUser.objects.create(
            username="ergash", first_name="Ergash", last_name="Quvondiqov", email="ergashquvondiqov0@gmail.com"
        )
        user.set_password("somepass")
        user.save()
        review1 = BookReview.objects.create(book=book, user=user, stars_given=3, comment="Very good book")
        review2 = BookReview.objects.create(book=book, user=user, stars_given=4, comment="Useful book")
        review3 = BookReview.objects.create(book=book, user=user, stars_given=5, comment="Nice book")

        response = self.client.get(reverse("home_page") + "?page_size=3")
        self.assertContains(response, review3.comment)
        self.assertContains(response, review2.comment)
