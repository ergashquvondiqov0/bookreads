from django.test import TestCase
from django.urls import reverse

from books.models import Book, BookReview
from users.models import CustomUser


class BookReviewAPITestCase(TestCase):
    def setUp(self):
        self.user = CustomUser.objects.create(username="ergash", first_name="Ergash")
        self.user.set_password("somepass")
        self.user.save()
        
    def test_book_review_detail(self):
        book = Book.objects.create(title="Book1", description="Desc1", isbn="1212")
        br = BookReview.objects.create(book=book, user=self.user, stars_given=5, comment="Very good")
        
        response = self.client.get(reverse('api:review-detail', kwargs={'id': br.id}))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data['id'], br.id)
        self.assertEqual(response.data['stars_given'], 5)
        self.assertEqual(response.data['comment'], 'Very good')
        self.assertEqual(response.data['book']['id'], br.book.id)
        self.assertEqual(response.data['book']['title'], 'Book1')
        self.assertEqual(response.data['book']['description'], 'Desc1')
        self.assertEqual(response.data['book']['isbn'], '1212')
        self.assertEqual(response.data['user']['id'], self.user.id)
        self.assertEqual(response.data['user']['first_name'], 'ergash')

