from django.core.mail import send_mail
from django.db.models.signals import post_save
from django.dispatch import receiver

from users.models import CustomUser


@receiver(post_save, sender=CustomUser)
def send_welcom_email(sender, instance, created, **kwargs):
    if created:
        send_mail(
            "Welcome to Bookreads",
            f"Hi, {instance.username}. Welcome to Bookreads. Enjoy the books and reviews",
            "ergashquvondiqov0@gmail.com",
            [instance.email]
        )